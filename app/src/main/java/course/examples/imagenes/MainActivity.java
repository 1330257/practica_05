package course.examples.imagenes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "course.examples.imagenes.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void enviarImagen(View v) {
        switch (v.getId()) {
            case (R.id.Button_1):
                Intent intent = new Intent(this, Imagen.class);
                String message1 = "1";
                intent.putExtra(EXTRA_MESSAGE, message1);
                startActivity(intent);

                break;
            case (R.id.Button_2):

                Intent intent2 = new Intent(this, Imagen.class);
                String message2 = "2";
                intent2.putExtra(EXTRA_MESSAGE, message2);
                startActivity(intent2);

                break;
            case (R.id.Button_3):

                Intent intent3 = new Intent(this, Imagen.class);
                String message3 = "3";
                intent3.putExtra(EXTRA_MESSAGE, message3);
                startActivity(intent3);

                break;

            case (R.id.Button_4):

                Intent intent4 = new Intent(this, Imagen.class);
                String message4 = "4";
                intent4.putExtra(EXTRA_MESSAGE, message4);
                startActivity(intent4);

                break;
        }


    }
}
